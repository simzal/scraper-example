<?php

namespace App\Plugins\Uk\ExampleSite;

use App\Plugins\Uk\ExampleSite\Contracts\Parser;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class EventParser implements Parser
{
	protected $html;
	protected $results = [];

    
    public function __construct(string $html)
    {
		$this->html = $html;
        $this->parse();
    }
    

    protected function parse()
    {
        // do something
        $this->results = [
        	'date' => '2019-01-01',
        	'name' => 'Joe Doe',
        	//etc
        ];
    }

    public function results(): array
    {
    	return $this->results;
    }
}
