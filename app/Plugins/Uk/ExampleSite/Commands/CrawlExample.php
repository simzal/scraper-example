<?php

namespace App\Plugins\Uk\ExampleSite\Commands;

use Illuminate\Console\Command;
use App\Plugins\Uk\ExampleSite\Crawler;

class CrawlExample extends Command
{
    protected $signature = 'crawl:example';

    protected $description = 'Crawl example data';

    public function handle()
    {
        new Crawler();
    }


}