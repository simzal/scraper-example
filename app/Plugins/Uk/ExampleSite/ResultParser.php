<?php

namespace App\Plugins\Uk\ExampleSite;

use App\Plugins\Uk\ExampleSite\Contracts\Parser;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class ResultParser implements Parser
{
    protected $html;
	protected $crawler;
	protected $results = [];
    
    public function __construct(string $html)
    {
		$this->html = $html;
        $this->crawler = new DomCrawler();
        $this->crawler->addHTMLContent($html, 'UTF-8');
        $this->parse();
    }
    

    protected function parse()
    {
        $name = $this->crawler->filter('h1')->html();
        $url = $this->crawler->filter('a')->attr('href');
        $this->results = compact('name', 'url');
    }

    public function results(): array
    {
    	return $this->results;
    }
}
