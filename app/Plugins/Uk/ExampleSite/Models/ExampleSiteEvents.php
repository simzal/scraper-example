<?php

namespace App\Plugins\Uk\ExampleSite\Models;

use Illuminate\Database\Eloquent\Model;

class ExampleSiteEvents extends Model
{
    protected $guarded = [];
}
