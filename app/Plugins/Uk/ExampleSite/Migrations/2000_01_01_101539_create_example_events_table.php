<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExampleEventsTable extends Migration
{

    public function up()
    {
        Schema::create('examplesite_events', function (Blueprint $table) {
            $table->increments('id');
            $table->date('event_id'); // site's id of event
            $table->date('date');
            $table->string('name');
            // etc
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('examplesite_events');
    }
}
