<?php

// use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
// use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    public function testReultsParser()
    {
    	$html = file_get_contents('https://example.com/');
        $results = (new \App\Plugins\Uk\ExampleSite\ResultParser($html))->results();
        $this->assertEquals($results, [
            'url' => 'http://www.iana.org/domains/example',
            'name' => 'Example Domain',
        ]);
    }
}
