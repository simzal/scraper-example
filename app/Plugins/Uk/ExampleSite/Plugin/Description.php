<?php 

namespace App\Plugins\Uk\ExampleSite\Plugin;

class Description{

	public $pluginName = 'ExampleSite'; // same as folder name
	public $table = 'examplesite_results'; // results table
	public $otherTables = ['examplesite_events'];
	/* how often should the crawler be started to get new things within a week */
	public $cron = '44 4 * * *'; 
	public $command = 'crawl:example'; // artisan command to be run by cron
	public $countries = ['uk'];
	public $proxy = true;
	public $delay = [20, 40];
	public $columns = [
		/* standard name => actual column name in examplesite_results */
		// 'name' => 'name',
		// 'surname' => 'last_name',
		'fullname' => 'name',
		// 'year_of_birth' => ,
		'date_from' => 'date_from',
		'date_to' => 'date_to',
		// 'league' => 'league',
		'link' => 'url',
	];
    /* standard names that we use are: name, surname, 
    fullname, date_from, date_to, link */
}