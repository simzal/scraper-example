<?php

namespace App\Plugins\Uk\ExampleSite\Models;

use Illuminate\Database\Eloquent\Model;

class ExampleSiteResults extends Model
{
    protected $guarded = [];
}
