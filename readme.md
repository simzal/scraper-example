# Task

## Overview

- See the PDF for details of things that we need to crawl
- We are making a scraper for one website, this scraper is part of a bigger Laravel program
- We need to crawl the whole website and extract all relevant data
- Exceptions are events older than 3 years and persons 16 years old or less
- The crawled data about events and persons should be stored into DB tables
- Crawler should support recrawl - overwrite the old data
- Crawler should be able to recrawl all data in one week

## Technical details

### Git repo
- Make a repository on Bitbucket or Github to privately store/share the code
- If you already worked for us, you can use the same repo as before

### Database tables

- name tables with lower snake case like: *LowerPluginName*_results, *LowerPluginName*_events . Make as many tables as you need, with prefix *LowerPluginName*_ .

### *LowerPluginName*_results table

- this is the main table to store the data. 
- It should contain fields "name" and "surname" or "fullname" (person we crawled),  "date" (or "date_from", "date_to") (of crawled event), "event_name", "link" (url of crawled webpage)
- *LowerPluginName*_results must also contain all the other data we want to crawl (red circles in our website photos) like "birth_year", "postal_code", "ranking", "city", "country" etc.
- Migration should make column "date" of type Date and indexed like this `$table->date('date')->index();`
- Migration should make column "fullname" a fulltext index like this: `DB::statement('ALTER TABLE *LowerPluginName*_results ADD FULLTEXT fullname(fullname)');`

### Folders

- All your code should be inside `app\Plugins\*CountryCode*\*PluginName*`, with subfolders `Commands`, `Logs`, `Migrations`, `Models`, `Plugin`, `Tests`.
- Do not make any controllers or views.
- If you need any helper functions make a Helper.php inside *PluginDir* and use static methods like Helper::cleanText($text).

### *PluginDir*/Commands/Crawl*PluginName*.php file
- command to run the crawler should be `crawl:*LowerPluginName*`

### *PluginDir*/Plugin/Description.php file

- in $cron you need to define how often the crawler should be run to extract the data to capture all changes on website within a week.
- $command is the artisan command that will trigger the crawl ( crawl:*LowerPluginName* )
- $table = '*LowerPluginName*_results';
- $otherTables = \[ '*LowerPluginName*_events' \]; // list other tables here


### Dependencies

- You need to use Laravel models to save/load data from DB
- You need to use Guzzle to fetch html data and Symfony DomCrawler to extract data from HTML.
- If you need some other external library please contact us first

### Coding standards

- Please try follow PHP-FIG PSR-1, PSR-2 standards for code.
- Scraper should have a class called `Crawler` that starts the crawl, it will be called like this: `new Crawl()`
- All your code should be in the beforementioned folder, there should be no views, controllers or other things


# Template with Example scraper

- You can find the crawler template here: https://bitbucket.org/simzal/scraper-example/
- This is fresh install of Laravel 5.8 with the scraper inside `App/Plugins/Uk/ExampleSite`
- You don't need to add console commands to Kernel.php, it will do this automatically.

## Install & run
```
git clone https://bitbucket.org/simzal/scraper-example.git scraper-example
cd scraper-example
composer install
php artisan crawl:example
```
