<?php

namespace App\Plugins\Uk\ExampleSite;

use GuzzleHttp\Client;
use App\Plugins\Uk\ExampleSite\Contracts\Crawl;

class CrawlResult implements Crawl
{
	protected $url;

	public function __construct(string $url)
	{
		$this->url = $url;
	}
}