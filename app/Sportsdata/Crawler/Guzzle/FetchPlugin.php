<?php 

namespace App\Sportsdata\Crawler\Guzzle;


class FetchPlugin{

	protected $pluginClass;

	public function __construct()
	{
		$this->handle();
	}

	protected function handle()
	{
		$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10);
		$classes = array_column($backtrace, 'class');
		foreach ($classes as $class) {
			if(starts_with($class, 'App\Sportsdata\Crawler')){
				continue;
			}
			break; // $class = first viable candidate
		}
		$this->setPluginClass($class);
	}

	public function plugin()
	{
		if($this->exists()){
			return app()->make($this->pluginClass);
		}
	}

	public function exists()
	{
		return class_exists($this->pluginClass);
	}

	protected function setPluginClass($class)
	{
		$chunks = explode('\\', $class, 4);
		// $chunks = explode('\\', 'App\Plugins\Ch\SwissTennis\PlayersCrawler',4);
		array_pop($chunks);
		$chunks[] = 'Plugin';
		$this->pluginClass = implode('\\', $chunks);
		// $this->pluginClass = class_exists($pluginClass) ? $pluginClass : null;
	}
	
}