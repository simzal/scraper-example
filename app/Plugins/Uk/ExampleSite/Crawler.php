<?php

namespace App\Plugins\Uk\ExampleSite;

use GuzzleHttp\Client;

class Crawler
{
    
    public function __construct()
    {
        $this->crawl();
    }

    public function crawl()
    {
        $this->fetch();
        echo "example site crawl finished";
        // get data
    }

    protected function fetch()
    {
        // get html
    }
}
