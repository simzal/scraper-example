<?php

namespace App\Plugins\Uk\ExampleSite;

use App\Plugins\Uk\ExampleSite\Contracts\Parser;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class EventListParser implements Parser
{
	protected $html;
	protected $results = [];

    public function __construct(string $html)
    {
		$this->html = $html;
        $this->parse();
    }
    
    protected function parse()
    {
        // extract something like this
        $this->results = [
            [
                'date' => '2019-01-01',
                'name' => 'Event 1',
                'url' => 'https://event1.com'
            ],
            [
                'date' => '2019-01-01',
                'name' => 'Event 1',
                'url' => 'https://event1.com'
            ]
        	//etc
        ];
    }

    public function results(): array
    {
    	return $this->results;
    }
}
