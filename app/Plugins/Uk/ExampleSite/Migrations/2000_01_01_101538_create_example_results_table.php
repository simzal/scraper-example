<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExampleResultsTable extends Migration
{

    public function up()
    {
        Schema::create('examplesite_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->date('event_date');
            $table->string('url');
            $table->string('name');
            $table->string('surname');
            $table->string('team');
            $table->string('event_type');
            // etc
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('examplesite_results');
    }
}
