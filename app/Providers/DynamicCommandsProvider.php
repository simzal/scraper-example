<?php

namespace App\Providers;

use File;
use Cache;
use App\Search\Plugins\Paths;
use Illuminate\Support\ServiceProvider;

class DynamicCommandsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $filepaths = $this->paths();
        foreach($filepaths ?? [] as $filepath){
            $this->fromFilepath($filepath);
        }
    }

    protected function paths()
    {
        $countryDir = '*';
        $subdir = 'Commands/*';
        $pluginDirs = File::glob(app_path("Plugins/{$countryDir}/*/" . $subdir));
        return $pluginDirs;
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {


    }

    protected function fromFilepath($filepath)
    {
        $commName = pathinfo($filepath)['filename'];
        $this->app->singleton('command.dynamic.' . $commName, 
            function ($app) use($filepath){
                $class = str_replace([base_path(), '.php', '/'], ['', '', '\\'], $filepath);
                $class = ucfirst(ltrim($class, '\\'));
                return $app[$class];
            });
        $this->commands('command.dynamic.' . $commName);
    }
}
